# Media Thumbnails Video

The Media Thumbnails Video module leverages the Media Thumbnails framework to 
generate thumbnails for video media entities. This module simplifies the 
process of creating and managing visually appealing thumbnails for videos, 
enhancing the user experience on your Drupal site.

For a full description of the module, visit the [project 
page](https://www.drupal.org/project/media_thumbnails_video).

Submit bug reports and feature suggestions, or track changes in the [issue queue](https://www.drupal.org/project/issues/media_thumbnails_video).


## Table of contents

- Requirements
- Recommended modules
- Installation
- Configuration
- Maintainers


## Requirements

This module requires the following modules:
- [Media Thumbnails](https://www.drupal.org/project/media_thumbnails)


## Installation

Install as you would normally install a contributed Drupal module. For further 
information, see 
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

1. Enable the module at Administration > Extend.
2. Navigate to Configuration > Media > Media Thumbnails Video Settings to 
   configure the module.
3. Enjoy automatically generated thumbnails for your video media entities.


## Maintainers

- Szczepan Musiał - [lamp5](https://www.drupal.org/u/lamp5)
